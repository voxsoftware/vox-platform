

// Archivo de ejemplo de configuración de proyecto de vox-core


export default {
		
	"name": "Hello world", 

	// Esto es obligatorio
	"id": "org.voxsoftware.example",

	// Parametros para output	
	"out": {
		// Por defecto es out 
		"path" : "out/", 
		"releases": [

			// Un array de los archivos que se debe generar ...
			{

				// Ejemplo de android 
				"platform": "android", 
				// Iconos de la aplicacíón
				"icons": [

					{
						"path": "./ico.png",
						"size": {
							"width": 64,
							"height": 64
						}
					}

				], 

				// Screens de la aplicación 
				"screens": [
					// En android e IOs se manejará screens
					{

						"path": "./screen.png", 
						"size": {
							"width": 1024,
							"height": 460
						}

					}
				]

				// Archivo .js, .es6 que se ejecuta como punto de arranque del programa
				"command": "cli.js"

			}

		]
	}, 

	"vox-core": {
		// Define una carpeta diferente a la instalada  de vox-core
		// Esto es para usar en plataformas que debe compilarse 
		// Aún no se va a implementar
		"path": ""
	}


}