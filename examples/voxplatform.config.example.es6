
// Archivo de configuración ejemplo para vox-platform


export default {
		
	"environment": {

		// En este espacio irán variables de environment
		// que se quieran usar al momento de ejecutar los subprocesos de compilación
		"VARIABLEEJEMPLO": "valor"

		// En este espacio no debería ir la variable PATH para evitar
		// inconvenientes al compilar 

	}, 


	"android": {
		// Aquí iría el path a android si lo requiere necesario
		// sin embargo la aplicación buscará Android en la variable PATH  de entorno
		"path": "/path/to/android", 

		// Son los archivos además de .npmignore y .voxignore
		// que se deben usar para definir archivos que no se copian 
		// aplicable cuando se compila android
		"ignorefiles": [".voxandroidignore"],
		
		// PLugins a instalar por defecto 
		// Debería tener cuidado al modificar esta sección
		// debería dejarla tal como está
		"plugins": [
			"io.jxcore.node",
			"cordova-plugin-crosswalk-webview"
		]
	},


	"cordova": {
		// Path al ejecutable de cordova es opcional ..
		"path": "/path/to/cordova"
	}, 



	"pluginCache": {

		// Caché de los plugins ...
		// En este objeto se puede definir rutas remotas para plugins que 
		// no están registrados directamente en npm

		"io.jxcore.node": {
			"name" :  "io.jxcore.node", 
			"local":  "", 
			"remote": "https://raw.githubusercontent.com/voxsoftware/vox-platform-releases/master/io.jxcore.node.json"
		}

	}



}