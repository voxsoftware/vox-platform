# vox-platform

vox-platform permite crear aplicaciones usando vox-core disponible en github. Internamente usa cordova, y jxcore-cordova. 
Una característica importante de vox-platform es que permite crear módulos nativos Ios, y Android que se pueden ejecutar directamente desde jxcore-cordova.

### Módulos Ios y Android para jxcore-cordova

jxcore-cordova permite añadir métodos de extensión, como lo indica su documentación.
vox-platform facilita esta tarea, haciendo que se puedan escribir métodos de extensión fácilmente y se añaden al proyecto automáticamente sin
necesidad de ejecutar un comando CLI. 
Por ejemplo en cordova se necesitaría algo como: `cordova plugin add xxx-plugin ` pero en vox-platform esto no es necesario, ya que 
en package.json se puede especificar estos archivos nativos y/o módulos cordova a añadir. Esto hace posible que usted desarrolle una aplicación
justo como lo haría para plataformas de escritorio, una aplicación nodejs. 

