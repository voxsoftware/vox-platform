
// Archivo para compilar el front-end con vox-webcompiler
var Path= require("path")
var out= Path.join(__dirname, 'out', 'script')
core.VW.Transpile(Path.join(__dirname, 'src', 'script'), out)
var minimal= core.VW.Web.Compiler.minimal
var path= Path.join(out, 'main.js')

var data = {
	node: {
	    "fs": "empty"
	},
	module: {
    	loaders: [
    		{ test: /\.json$/, loader: "json-loader" }
    	]
  	},
    entry:  path,
    output: {
        path:  Path.join(__dirname,"dist") ,
        filename: minimal?"js/app.min.js":"js/app.js",
        libraryTarget: "umd"
    }
}
module.exports= data