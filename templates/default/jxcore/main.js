
var vox= require("./vox-core")

// Registrar funciones
var copy= {

	"info": console.info,
	"warning": console.warn, 
	"error": console.error, 
	"log": console.log, 
	"vwlog": vw.log, 
	"vwinfo": vw.info, 
	"vwerror": vw.error, 
	"vwwarning": vw.warning

}


var createFunction= function(func, self, mobileFunc){
	return function(){
		var a, e, v= func.apply(self, arguments)

		for(var i=0;i<arguments.length;i++){
			e= arguments[i]
			if(e instanceof Error){
				a= {
					stack: e.stack, 
					message: e.message
				}
				for(var id in e){
					a[id]= e[id]
				}
				arguments[i]= a
			}
		}
		Mobile(mobileFunc).call(Array.prototype.slice.call(arguments, 0, arguments.length))
		return v
	}
}


console.info= createFunction(copy.info, console, "console.info")
console.log= createFunction(copy.log, console, "console.log")
console.warn= createFunction(copy.warn, console, "console.warning")
console.error= createFunction(copy.error, console, "console.error")
vw.info= createFunction(copy.vwinfo, vw, "console.info")
vw.log= createFunction(copy.vwlog, vw, "console.log")
vw.error= createFunction(copy.vwerror, vw, "console.error")
vw.warning= createFunction(copy.vwwarning, vw, "console.warning")

var app= require("./cli")
module.exports= core.VW.Mobile
