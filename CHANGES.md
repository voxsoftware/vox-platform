## Historia de cambios

### [v0.0.1]

> Mayo 18, 2016

- **Ícono**: Se crea ícono de vox-platform. PSD y versiones PNG
- **Plataforma CLI**: Se crea plataforma CLI para la creación, preparación y compilación de aplicaciones basadas en *vox-core*
- **Versión inicial**: Estoy complacido de presentar la primera versión de vox-platform.
