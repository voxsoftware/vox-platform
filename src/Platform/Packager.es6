var D= core.org.voxsoftware.Developing
var Fs= core.System.IO.Fs
import Path from 'path'
import fs from 'fs'
// Clase abstracta para los empaquetadores de aplicaciones ...
class Packager extends D.Configuration{
	get cordova(){
		return this.parent.cordova
	}
	static get defaultConfig(){
		return {
		}
	}


	get workingDir(){
		var home= Path.join(this.project.directoryName, ".voxplatform")
		if(!Fs.sync.exists(home))
			Fs.sync.mkdir(home)

		return home
	}


	get workingProjectDir(){
		return Path.join(this.workingDir, ".cordova")
	}



	get configurationFile(){
		var home= this.workingDir

		home= Path.join(home, "project.json")
		if(!Fs.sync.exists(home))
			Fs.sync.writeFile(home, core.safeJSON.stringify(Packager.defaultConfig,null,'\t'))

		return home
	}
	get filename(){
		return this.configurationFile
	}


	get project(){
		return this.$project
	}


	set project(pro){
		var name= this.platformName
		this.$output= pro.getOutputFor(name)
		if(!this.$output)
			throw new core.System.NotImplementedException("El proyecto no soporta la plataforma: " + this.platformName)

		this.$project= pro
		var file= this.configurationFile
		if(Fs.sync.exists(file))
			D.Configuration.call(this, file, this.$packageManager)
		else
			D.Configuration.call(this, Packager.defaultConfig, this.$packageManager)

		
	}


	getOptions({cwd}){
		var env= {}, env2
		for(var id in process.env){
			env[id]= process.env[id]
		}
		env2= this.parent.environment
		for(var id in env2){
			env[id]= env2[id]
		}

		if(!env.ANDROID_HOME)
			env.ANDROID_HOME= this.parent.androidHome||""

		if(!env.JAVA_HOME)
			env.JAVA_HOME= this.parent.javaHome||""




		return {
			"env": env, 
			"stderrAsError": true, 
			"cwd": cwd || this.workingProjectDir, 
			"emitter": this
		}

	}


	get plugins(){
		var p= this.get("plugins")
		if(!p)
			this.set("plugins", p={})
		return p
	}

	set plugins(p){
		return this.set("plugins", p)
	}




	



	constructor(packageManager){
		this.$packageManager=packageManager
		//var file= this.configurationFile
		super({}, packageManager)
	}



	// Crear el  proyecto de cordova 
	async createProject(){
		var name= this.project.name
        var id= this.project.id
        var nocrear= false
        var path= Path.basename(this.workingProjectDir)
        if(!name)
            throw new core.System.ArgumentException("Debe especificar el argumento `name`, nombre del proyecto",'options.name')
        if(!id)
            throw new core.System.ArgumentException("Debe especificar el argumento `id`, id del proyecto",'options.id')


        var content, projectDir= this.workingProjectDir
       	//vw.info("HERE", projectDir)
        if(Fs.sync.exists(projectDir)){

            contents= await Fs.async.readdir(projectDir)
            if(contents.length>0){
                //throw new D.Platform.DirectoryNotEmptyException(`El directorio "${projectDir}" no está vacío`)
                nocrear= true
            }	
			/*else if(contents.length==1){
				if(!Fs.sync.exists(this.configurationFile))
					throw new D.Platform.DirectoryNotEmptyException(`El directorio "${projectDir}" no está vacío`)
				else
					nocrear= true
			}            */
        }

        if(!nocrear){
        	var cmd= ["create", path, id, name]
        	
        	await this.cordova.executeAsync(cmd, this.getOptions({
        		"cwd": this.workingDir
        	}))
        	this.emit("packager:init")
        	this.save()
        }

        try{
	        await this.cordova.executeAsync(["platform", "add", this.platformName], this.getOptions())
	        this.emit("platform:added", this.platformName)
        }
        catch(e){}


	}






	// async 
	// comando para iniciar  
	init(){
		throw new core.System.NotImplementedException()
	}

	// async 
	// comando para preparar la aplicación
	prepare(){
		throw new core.System.NotImplementedException()
	}


	// async 
	// comando para compilar la aplicación
	async compile(){
		//throw new core.System.NotImplementedException()
		var output= this.project.getOutputFor(this.platformName), folderWriter
		await this.cordova.executeAsync(["prepare", this.platformName], this.getOptions())
		try{
			await this.cordova.executeAsync(["compile", this.platformName], this.getOptions())
		}catch(e){

		}

		var out, apkPath= Path.join(this.workingProjectDir, "platforms", this.platformName, 
			"build", "outputs", "apk")

		if(Fs.sync.exists(apkPath)){
			out= Path.resolve(this.project.outPath || "out")
			if(!Fs.sync.exists(out))
				Fs.sync.mkdir(out)

			folderWriter= new D.Platform.FolderWriter(apkPath)
			await folderWriter.copyTo(out)

		}

		this.emit("package:compiled", this.platformName)
	}



}

export default Packager