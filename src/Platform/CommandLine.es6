
import Path from 'path'
var VoxPlatform= core.org.voxsoftware.Developing.Platform
var D= core.org.voxsoftware.Developing
var Cli
class CommandLine{


	static prompt(){

		core.VW.Console.foregroundColor= core.System.ConsoleColor.Green
		core.VW.Console.write("Vox Platform ")
		core.VW.Console.resetColors()
		core.VW.Console.write("versión ")
		core.VW.Console.foregroundColor= core.System.ConsoleColor.Yellow
		core.VW.Console.write(core.org.voxsoftware.Developing.version,"")
		core.VW.Console.resetColors()
	}


	static error(e){

		core.VW.Console.backgroundColor= core.System.ConsoleColor.Red
		core.VW.Console.foregroundColor= core.System.ConsoleColor.White
		core.VW.Console.write(" ERROR ")
		core.VW.Console.resetColors()
		core.VW.Console.foregroundColor= core.System.ConsoleColor.Yellow
		core.VW.Console.write("", e.stack||e.toString())
		core.VW.Console.writeLine()
	}
	static cli(){

		var Command=new  core.VW.CommandLine.Parser()
		Command.addParameter("prepare")
		Command.addParameter("init")
		Command.addParameter("compile")
		Command.addParameter("help")
		try{
			Command.parse()
			var options= Command.getAsOptionsObject()
		}
		catch(e){
			Cli.prompt()
			core.VW.Console.writeLine()
			return Cli.error(e)
		}
		return Cli.execute(options)

	}


	static async execute(options){

        try{
			if(options.init){
    			await Cli.init(options)
    		}
			else if(options.prepare){
    			await Cli.prepare(options)
    		}
    		else if(options.compile){
                await Cli.compile(options)
    		}
    		else{
    			Cli.help()
    		}
        }
        catch(e){

			core.VW.Console.writeLine()
			return Cli.error(e)
        }
	}

    static config(options){
        if(options["config-file"])
            config= new VoxPlatform.Config(options["config-file"])

		else
        	config= new VoxPlatform.Config()

		var per,per0
		config.on("npmloading", function(ev){
			if(ev.firsttime){
				core.VW.Console.writeLine("Se está preparando NPM para el primer uso")
			}
			else{
				per=ev.percent|0
				if(per!=per0 && (per %20==0))
					core.VW.Console.setColorWarning().writeLine("- Preparando: ", per, "%").resetColors()

				per0=per
				if(per==100)
					core.VW.Console.writeLine()
			}
		})

		return config
    }


    static attachEvents(packager){

    	packager.on("cordova:stdout", (val)=>{
    		core.System.Console.foregroundColor= core.System.ConsoleColor.Gray
    		core.VW.Console.write(val.toString()).resetColors()
    	})

    	packager.on("cordova:stderr", (val)=>{
    		core.System.Console.foregroundColor= core.System.ConsoleColor.Red
    		//core.System.Console.backgroundColor= core.System.ConsoleColor.White
    		core.VW.Console.write(val.toString()).resetColors()
    	})

    	packager.on("packager:init", ()=>{
    		vw.log("Empaquetador iniciado correctamente")
    	})

    	packager.on("platform:added", (p)=>{
    		vw.log("Plataforma agregada: ", p)
    	})

    	
    	packager.on("plugin:add", (p)=>{
    		vw.log("Plugin agregado correctamente: ", p)
    	})

    	packager.on("plugin:downloading", (p)=>{
    		vw.log("Se está descargando datos de un plugin desde: ", p.downloadurl)
    	})

    	packager.on("plugins:adding", (p)=>{
    		vw.warning("Se están añadiendo plugins en caso de ser necesarios espere un momento ...")
    	})

    	packager.on("package:compiled", (p)=>{
    		vw.log("El proceso ha terminado. Por favor revise la salida para verificar si el proceso terminó satisfactoriamente")
    	})

    }
	

	static async init(options){
    
		var path= Path.resolve("")
		var platform= options.values[0]
		if(!platform)
			throw new core.System.ArgumentException("Debe especificar la plataforma  a usar")


		var project= new D.Project.Project(path)
		var manager= D.Platform.PackageManager.default
		var packager= manager.getPackager(platform)
		packager.project= project
		Cli.attachEvents(packager)

		await packager.init()
    }

    
    static async prepare(options){
    
		var path= Path.resolve("")
		var platform= options.values[0]
		if(!platform)
			throw new core.System.ArgumentException("Debe especificar la plataforma  a usar")


		var project= new D.Project.Project(path)
		var manager= D.Platform.PackageManager.default
		var packager= manager.getPackager(platform)
		packager.project= project
		Cli.attachEvents(packager)

		await packager.prepare()
    }


    static async compile(options){
    	
    	try{
			var path= Path.resolve("")
			var platform= options.values[0]
			if(!platform)
				throw new core.System.ArgumentException("Debe especificar la plataforma  a usar")


			var project= new D.Project.Project(path)
			var manager= D.Platform.PackageManager.default
			var packager= manager.getPackager(platform)
			packager.project= project
			Cli.attachEvents(packager)

			await packager.compile()
		}catch(e){
			core.VW.Console.writeLine()
			throw e
		}
    }


	static get options(){
		return {
			
		}
	}

	static get commands(){
		return {
			"-help": "Mostrar ayuda",
			"-init": "Configurar el proyecto para empezar a usarlo",
			"-compile": "Compilar el proyecto",
			"-prepare": "Preparar proyecto",
		}
	}



	static help(){
		var help=Cli.options
		var cmds=Cli.commands

		Cli.prompt()
		core.VW.Console.writeLine()
		core.VW.Console.writeLine()

		vw.warning("Modo de uso:")
		core.VW.Console.writeLine("  comando [opcion [argumento], opcion [argumento] ...] [argumentos]")


		core.VW.Console.writeLine()
		vw.warning("Comandos:")
		var maxl=0
		for(var id in help){
			maxl= Math.max(maxl, id.length)
		}
		for(var id in cmds){
			maxl= Math.max(maxl, id.length)
		}
		maxl+= 5

		for(var id in cmds){
			core.VW.Console.setColorLog().write(("  " + id).padRight(maxl,' ')).resetColors()
			core.VW.Console.writeLine(cmds[id])
		}


		core.VW.Console.writeLine()
		vw.warning("Opciones:")
		for(var id in help){
			core.VW.Console.setColorLog().write(("  " + id).padRight(maxl,' ')).resetColors()
			core.VW.Console.writeLine(help[id])
		}

	}


}
Cli= CommandLine
export default CommandLine
