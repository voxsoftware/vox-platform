import Path from 'path'
import Os from 'os'
var D= core.org.voxsoftware.Developing
var Fs= core.System.IO.Fs

class PackageManager extends D.Configuration{
	
	/**
	constructor(file){
		if(!file)	{
			file= PackageManager.defaultConfigurationFile
			arguments[0]= file
		}

		D.Configuration.apply(this, arguments)

	}*/


	find(name){

		var path= this.environment.PATH || process.env.PATH
		var delimiter= Path.delimiter
		var p, paths= path.split(delimiter)
		for(var i=0;i<paths.length;i++){
			p= paths[i]
			if(Os.platform()=="win32")
				l= PackageManager._findWin32(p, name)
			else
				l= PackageManager._findUnix(p, name)



			if(l)
				return l
		}

	}


	static _findUnix(path, name){
		path= Path.join(path, name)
		if(Fs.sync.exists(path))
			return path
	}


	static _findWin32(path, name){
		var options= [
			Path.join(path, name+".exe"),
			Path.join(path, name+".bat"),
			Path.join(path, name+".cmd")
		]

		for(var i=0;i<options.length;i++){
			if(Fs.sync.exists(options[i]))
				return options[i]
		}
	}


	get pluginCache(){
		var c, cache=  this.get("pluginCache")||[]
		for(var i in cache){
			c= cache[i]
			if(!(c instanceof D.Platform.PluginCache)){
				c= new D.Platform.PluginCache(c, this)
				cache[i]= c
			}
		}

		return cache
	}

	set pluginCache(v){
		return this.set("pluginCache", v)
	}


	 
	get cordovaConfig(){
		return this.get("cordova")
	}


	set cordovaConfig(v){
		return this.set("cordova",v)
	}

	get androidConfig(){
		return this.get("android")
	}

	set androidConfig(v){
		return this.set("android", v)
	}


	get environment(){
		var env=this.get("environment")
		if(!env){
			this.environment= env= {}
		}

		return env
	}

	set environment(v){
		return this.set("environment",v)
	}


	getDirname(path){
		return Path.dirname(Fs.sync.realpath(path))
	}



	get androidPath(){
		var home, path= this.get("android.path")
		if(!path){
			home= this.environment.ANDROID_HOME
			if(home){
				path= Path.join(home, "tools", "android")
			}
		}


		if(!path)
			path=this.find("android")


		return path
	}


	get cordovaHome(){
		var home= this.environment.CORDOVA_HOME || process.env.CORDOVA_HOME,e
		if(!home && (e=this.cordovaPath)){
			home=this.getDirname(e)
		}
		return home
	}


	get javaHome(){
		var home= this.environment.JAVA_HOME || process.env.JAVA_HOME,e
		if(!home && (e=this.javaPath)){
			home= Path.join(this.getDirname(e),"..")
		}
		return home
	}

	get androidHome(){
		var home= this.environment.ANDROID_HOME || process.env.ANDROID_HOME,e
		if(!home && (e=this.androidPath)){
			home= Path.join(this.getDirname(e),"..")
		}
		return home
	}


	get cordovaPath(){
		var path= this.get("cordova.path")
		if(!path)
			path=this.find("cordova")

		return path
	}

	get javaPath(){
		var path= this.get("java.path")
		if(!path)
			path=this.find("java")

		return path
	}




	getPackager(platform){
		platform= platform||""
		if(platform.toLowerCase()=="android")
			return new D.Platform.AndroidPackager(this)


		throw new core.System.NotImplementedException("La plataforma " + plataform+ " no se ha implementado")

	}



	get cordova(){
		if(!this.$.cordova)
			this.$.cordova= new D.Platform.Cordova(this.cordovaConfig, this)

		return this.$.cordova
	}


	static get defaultConfigurationFile(){


		var home= process.env.HOME || process.env.USERPROFILE
		var config= Path.join(home, ".voxsoftware")
		if(!Fs.sync.exists(config))	
			Fs.sync.mkdir(config)

		config= Path.join(config, "voxplatform")
		if(!Fs.sync.exists(config))	
			Fs.sync.mkdir(config)

		var file= Path.join(config, "config.json")
		if(!Fs.sync.exists(file))
			Fs.sync.writeFile(file, core.safeJSON.stringify(PackageManager.defaultConfiguration,null,'\t'))


		return file

	}


	static get ["default"](){

		return new PackageManager(PackageManager.defaultConfigurationFile)
	}

	static get defaultConfiguration(){

		return {
			"environment": {}, 
			"android": {

				"ignorefiles": [".voxandroidignore", ".mobileignore"],
				"plugins": [
					"io.jxcore.node",
					"cordova-plugin-crosswalk-webview"
				]

			}, 

			// Por ahora no implementado 
			"ios": {

			}, 

			"cordova": {},
			"pluginCache": {

				// Caché de los plugins ...
				// En este objeto se puede definir rutas remotas para plugins que 
				// no están registrados directamente en npm

				"io.jxcore.node": {
					"name" :  "io.jxcore.node", 
					"local":  "", 
					"remote": "https://raw.githubusercontent.com/voxsoftware/vox-platform-releases/master/io.jxcore.node.json"
				}

			}
		}

	}


}
export default PackageManager