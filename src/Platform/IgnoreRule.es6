import Path from 'path'
import {Minimatch} from 'minimatch'
var Fs= core.System.IO.Fs


class IgnoreRule{
	

	constructor(file, relativeTo, rule){
		this.$={}
		this.$.rule= rule
		this.$.file= file
		this.$.dirname= Path.dirname(file)
		this.$.relativeTo= relativeTo
		this.$.relative= Path.relative(this.$.relativeTo, this.$.dirname)
	}

	get minimatch(){
		if(!this.$.minimatch){
			//vw.error(this.$.rule.pattern)
			this.$.minimatchO= Minimatch(this.$.rule.pattern)
			this.$.minimatch= this.$.minimatchO.match.bind(this.$.minimatchO)
		}

		return this.$.minimatch
	}

	check(file, toRelative, directory){
		var stat
		var relative= toRelative? Path.relative(toRelative, file): file
		if(this.$.rule.globalsearch){
			relative= Path.basename(relative)
		}
		else{
			relative= Path.relative(this.$.relative, relative)
		}

		//vw.log("RUl:", ",", file, ",", toRelative, ",", this.$.rule.pattern)
		var match= this.minimatch(relative)
		if(match){
			/*if(this.$.rule.directory){
				stat= Fs.sync.stat(file)
				match= stat.isDirectory() 
			}*/
			if(this.$.rule.directory && !directory)
				match= false

		}

		return match
	}


}
export default IgnoreRule