import {Minimatch} from 'minimatch'
import Path from 'path'
var Fs= core.System.IO.Fs
var D= core.org.voxsoftware.Developing


class FolderWriter{


	
	constructor(src){
		this.$={}
		this.$.src= src
		this.$.ignores= []
		this.$.notignores= []
		this.ignoreFiles= [".npmignore", ".voxignore"]
		this.addFiles= [".voxnotignore"]
		this.addIgnore({
			"globalsearch": true, 
			"pattern": ".*",
			"file": ".virtualignore"
		})
	}

		
	load(){
		return this.__revisarIgnores()
	}



	get ignores(){
		return this.$.ignores
	}

	get notIgnores(){
		return this.$.notignores
	}



	async __revisarIgnores(src){

		if(this.$.revised)
			return 

		if(!src)
			src= this.$.src

		var ufile, stat, file, files
		files= await Fs.async.readdir(src)
		//vw.info(this.ignoreFiles)
		//process.exit()
		for(var i=0;i<files.length;i++){
			file= files[i]
			ufile= Path.join(src, file)
			stat= await Fs.async.stat(ufile)

			//vw.error("FILE:",file, this.ignoreFiles)
			if(stat.isDirectory())
				await this.__revisarIgnores(ufile)
			else if(this.ignoreFiles.indexOf(file)>=0){
				await this.__addIgnores(ufile)
			}
			else if(this.addFiles.indexOf(file)>=0){
				await this.__addNotIgnoreFiles(ufile)
			}
		}
		this.$.revised= true
	}


	async __addNotIgnoreFiles(file){

		//vw.error(file)
		var content= await Fs.async.readFile(file,'utf8')
		var ignores= D.Platform.Util.parseIgnores(content), ignore

		/*
		var ignoreGroup= {}
		ignoreGroup.filename= file
		ignoreGroup.dirname= Path.dirname(file)
		ignoreGroup.ignores= ignores
		this.$.ignores.push(ignoreGroup)
		*/

		for(var i=0;i<ignores.length;i++){
			ignore= new D.Platform.IgnoreRule(file, this.$.src, ignores[i])
			//vw.log(ignores[i])
			this.$.notignores.push(ignore)
		}

	}


	async __addIgnores(file){

		//vw.error(file)
		var content= await Fs.async.readFile(file,'utf8')
		var ignores= D.Platform.Util.parseIgnores(content), ignore

		/*
		var ignoreGroup= {}
		ignoreGroup.filename= file
		ignoreGroup.dirname= Path.dirname(file)
		ignoreGroup.ignores= ignores
		this.$.ignores.push(ignoreGroup)
		*/

		for(var i=0;i<ignores.length;i++){
			ignore= new D.Platform.IgnoreRule(file, this.$.src, ignores[i])
			//vw.log(ignores[i])
			this.$.ignores.push(ignore)
		}

	}



	addIgnore(ignoreRule){
		if(!(ignoreRule instanceof D.Platform.IgnoreRule))
			ignoreRule= new D.Platform.IgnoreRule(ignoreRule.file, this.$.src, ignoreRule)		
		this.$.ignores.push(ignoreRule)
	}
	addNotIgnore(ignoreRule){
		if(!(ignoreRule instanceof D.Platform.IgnoreRule))
			ignoreRule= new D.Platform.IgnoreRule(ignoreRule.file, this.$.src, ignoreRule)
		this.$.notignores.push(ignoreRule)
	}


	async copyTo(dest){
		await this.__revisarIgnores()
		return await this.__copy(this.$.src, dest)
	}


	async copyFile(src, dest){
		var er,s1,s2

		try{
			s1= new core.System.IO.FileStream(src,  core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)
			s2= new core.System.IO.FileStream(dest, core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.ReadWrite)
			s2.setLength(0)
			await s1.copyToAsync(s2)
		}
		catch(e){
			er= e
		}

		if(s1)
			s1.close()
		if(s2)
			s2.close()	

		if(er)
			throw er
	}


	async __copy(src, dest, originalDest){


		if(!originalDest)
			originalDest= dest


		var files, file, ufile, stat, match, dfile
		files= await Fs.async.readdir(src)
		if(!Fs.sync.exists(dest))
			Fs.sync.mkdir(dest)

		for(var i=0;i<files.length;i++){
			file= files[i]
			ufile= Path.join(src, file)
			dfile= Path.join(dest, file)
			stat= await Fs.async.stat(ufile)


			match= false
			for(var y=0;y<this.$.ignores.length;y++){
				ignore= this.$.ignores[y]
				match= ignore.check(dfile, originalDest, stat.isDirectory())
				if(match)
					break
			}

			if(match){

				//vw.error(ufile)
				// Verificar las reglas notignore
				// Realmente esto solo tiene uso práctico en los template ...
				// en los demás casos solo debería usar las reglas ignores

				for(var y=0;y<this.$.notignores.length;y++){
					ignore= this.$.notignores[y]
					
					if(ignore.check(dfile, originalDest, stat.isDirectory())){
						//vw.warning(ignore, dfile, originalDest)
						match= false
					}
				}
			}

			if(!match){

				if(stat.isDirectory()){
					await this.__copy(ufile, dfile, originalDest)
				}
				else{
					await this.copyFile(ufile, dfile)
				}

			}

		}


	}


	
}
export default FolderWriter