
// PluginCache es una clase para administrar la configuración de caché 
// de los plugins cordova ...

import Path from 'path'
import Url from 'url'
import fs from 'fs'
var Fs= core.System.IO.Fs
var D= core.org.voxsoftware.Developing


class PluginCache extends D.Configuration{
	
	
	/**

	La configuración del caché de plugins lucirá algo como: 

	{
		
		"nombreplugin": {
			
			"name": "nombreplugin", 
			"local": "localpathtoplugin format .zip", 
			"remote": "remotepathtoplugin json ",
			"date" : Date() , 
			"localversion": "version"

		}		

	}


	*/


	get name(){
		return this.get("name")
	}

	set name(v){
		return this.set("name",v)
	}

	get local(){
		return this.get("local")
	}

	set local(path){
		return this.set("local", path)
	}

	get localversion(){
		return this.get("localversion")
	}

	set localversion(path){
		return this.set("localversion", path)
	}


	get remote(){
		return this.get("remote")
	}

	set remote(v){
		return this.set("remote",v)
	}

	get date(){
		return this.get("date")
	}

	set date(v){
		return this.set("date", v)
	}

	get cacheDirectory(){
		var path= this.root.directoryName
		path= Path.join(path, ".cache")
		if(!Fs.sync.exists(path))
			Fs.sync.mkdir(path)

		return path
	}



	__versionToNumber(version){
		if(!version)
			return 0
		var parts= version.split(".")
		parts= parts.map( (e)=> e|0 )
		return parts[0]* 1000000 + parts[1]*1000 + parts[2]
	}


	async refreshCacheIfNeeded({emitter}){

		// Actualiza el caché ...
		var report= {}, req
		var remote= this.remote
		var localversion= this.localversion
		var remoteinfo= await this.getInfoFromRemote(remote)
		if(!remoteinfo.latest)
			report.remotefailed= true

		report.notcached= !localversion
		report.local={
			version: localversion,
			"versionnumber": this.__versionToNumber(localversion)
		}
		if(remoteinfo){
			remoteinfo= report.remoteinfo= remoteinfo.latest
			report.remoteinfo.versionnumber= this.__versionToNumber(report.remoteinfo.version)
		}


		var urlzip= Url.resolve(remote, report.remoteinfo.path)
		//vw.error(remoteinfo)
		report.downloadurl= urlzip
		var name
		if(remoteinfo && remoteinfo.versionnumber> report.local.versionnumber){
			name= Path.join(this.cacheDirectory, this.name + "." + report.remoteinfo.version + ".zip")
			
			// Descargar el zip ...

			try{

				if(emitter)
					emitter.emit("plugin:download", report)
				req= new core.VW.Http.Request(urlzip)
				req.beginGetResponse()
				req.innerRequest.pipe(fs.createWriteStream(name))
				await req.endGetResponse()


				report.local.version= this.localversion= remoteinfo.version
				report.local.versionnumber= this.__versionToNumber(report.local.version)
				this.local= name
				await this.save()
			}
			catch(e){
				report.downloaderror= e
			}
		}
		else if(remoteinfo){
			report.notneeded= true
		}
		return report

	}


	async getInfoFromRemote(url){
		var req= new core.VW.Http.Request(url)
		var response= await req.getResponseAsync()
		var json= response.body
		if(typeof json =="string")
			json= JSON.parse(json)

		return json
	}


}
export default PluginCache