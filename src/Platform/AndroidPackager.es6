var D= core.org.voxsoftware.Developing
var Fs= core.System.IO.Fs
import Path from 'path'
class AndroidPackager extends D.Platform.Packager{



	async init(){

		// Crear el proyecto si es necesario 
		await this.createProject()

		// Verificar que tenga los plugins configurados como necesarios ...
		var androidConfig= this.parent.androidConfig

		this.emit("plugins:adding")
		var plugins= androidConfig.plugins ||[], plugin
		if(plugins.length>0){
			// Añadir los plugins ...
			for(var i=0;i<plugins.length;i++){
				plugin= plugins[i]
				//vw.warning(plugin)
				await this.addPlugin(plugin)
			}
		}

	}

	get platformName(){
		return "android"
	}


	async addPlugin(plugin){


		var pluginCache= this.parent.pluginCache
		var name= plugin
		var cache= pluginCache[name], report, zipFile, zip, pluginPath, files, alternativePluginPath

		// Si no es necesario instalar el plugin 
		if(this.plugins[name])
			return 

		if(cache){

			// Descarga él plugin y lo almacena en la caché si es necesario 
			//vw.info(cache)
			report= await cache.refreshCacheIfNeeded()
			if(!report.local.version)
				throw new core.System.Exception("El plugin " + name + " no se puede procesar y por tanto no se puede continuar. "+
						(report.downloaderror?report.downloaderror.message:""))


			if(report.downloaderror){
				this.emit("packager:pluginoutofversion", report)
			}


			zipFile= cache.local
			if(!zipFile)
				throw new core.System.Exception("Error desconocido al procesar el plugin " + name )


			pluginPath= Path.join(this.workingProjectDir, name)
			alternativePluginPath= pluginPath + Date.now().toString(32)
			zip= new core.System.Compression.ZipFile(zipFile)
			if(!Fs.sync.exists(pluginPath))
				Fs.sync.mkdir(pluginPath)
			zip.extractAllTo(pluginPath)



			files=  await Fs.async.readdir(pluginPath)
			if(files.length==1){
				Fs.sync.rename(pluginPath, alternativePluginPath)
				Fs.sync.rename(Path.join(alternativePluginPath, files[0]), pluginPath)
				//Fs.sync.unlink(alternativePluginPath)
				await D.Platform.Util.removeAsync(alternativePluginPath)
			}


		}


		// Añadir el plugin ...
		pluginPath= pluginPath|| name

		//vw.info("Añadiendo", pluginPath)

		await this.cordova.executeAsync(["plugin", "add", pluginPath], this.getOptions())
		this.emit("plugin:add", plugin)

		var p=this.plugins[name]= this.plugins[name]||{}
		p.name= name
		p.date= Date.now()
		p.active=true
		this.save()

	}



	async prepare(){


		var androidConfig= this.parent.androidConfig
		var ignoreFiles= androidConfig.ignorefiles
		
		var folderWriter= new D.Platform.FolderWriter(core.VW.path)
		if(ignoreFiles){
			for(var i=0;i<ignoreFiles.length;i++){
				folderWriter.ignoreFiles.push(ignoreFiles[i])
			}
		}


		// Copiar vox-core
		var wwwPath= Path.join(this.workingProjectDir, "www")
		if(Fs.sync.exists(wwwPath))
			await D.Platform.Util.removeAsync(wwwPath)

		if(!Fs.sync.exists(wwwPath))
			Fs.sync.mkdir(wwwPath)
		var jxcorePath= Path.join(wwwPath, "jxcore")		
		Fs.sync.mkdir(jxcorePath)
		var voxcorePath= Path.join(jxcorePath, "vox-core")
		Fs.sync.mkdir(voxcorePath)
		var appPath= Path.join(jxcorePath, "app")
		Fs.sync.mkdir(appPath)


		// Copiar los archivos
		await folderWriter.copyTo(voxcorePath)


		folderWriter= new D.Platform.FolderWriter(this.project.directoryName)
		if(ignoreFiles){
			for(var i=0;i<ignoreFiles.length;i++){
				folderWriter.ignoreFiles.push(ignoreFiles[i])
			}
		}
		await folderWriter.copyTo(appPath)

		// Copiar template ...
		await this.copyTemplate(wwwPath)



		var output= this.project.getOutputFor(this.platformName)
		var str, requ, cliPath
		requ= "./app/"
		if(output.command){
			requ+= output.command
		}
		cliPath= Path.join(jxcorePath, "cli.js")
		str= "module.exports= require(" + JSON.stringify(requ) + ")"
		await Fs.async.writeFile(cliPath, str)

	}	


	get template(){
		return this.get("template")
	}

	set template(v){
		return this.set("template", v)
	}

	async copyTemplate(dest, templatename, copyScreens= true, notIgnores=[]){

		var androidConfig= this.parent.androidConfig
		var ignoreFiles= androidConfig.ignorefiles


		var output= this.project.getOutputFor(this.platformName)
		//vw.info(output)
		var template= templatename || this.template || output.get("template") || "default"
		var path
		try{
			path= require.resolve(template)
		}
		catch(e){
		}

		if(!path)
			path= require.resolve(Path.join(__dirname, "..", "..", "templates", template))

		var templateInfo= require(path)
		//vw.log(templateInfo)
		path= Path.dirname(path)

		if(templateInfo.path)
			path= templateInfo.path

		var folderWriter= new D.Platform.FolderWriter(path)
		await folderWriter.load()


		if(templateInfo.base)
			await this.copyTemplate(dest, templateInfo.base, false, folderWriter.notIgnores)

		// Copiar a la carpeta 


		if(ignoreFiles){
			for(var i=0;i<ignoreFiles.length;i++){
				folderWriter.ignoreFiles.push(ignoreFiles[i])
			}
		}

		// Si el template hijo añade reglas notIgnores
		// para reemplazar las reglas ignore del padre 
		if(notIgnores && notIgnores.length>0){
			for(var i=0;i<notIgnores.length;i++){
				folderWriter.addNotIgnore(notIgnores[i])
			}
		}


		//if(templatename=="default")
		//	vw.info(folderWriter.notIgnores)

		
		await folderWriter.copyTo(dest)

		if(!copyScreens)
			return 

		var screenLand, screenPort, imgPath, screenLandPath, screenPortPath
		screenLand= output.getNearestScreen({
			"width": 1920,
			"height": 1080
		}) || D.Project.Screen.landscape

		screenPort= output.getNearestScreen({
			"width": 1080,
			"height": 1920
		}) || D.Project.Screen.portrait

		imgPath= Path.join(dest, "img")
		screenLandPath= Path.join(imgPath, "screen-land.png")
		screenPortPath= Path.join(imgPath, "screen-port.png")
		if(!Fs.sync.exists(imgPath))
			Fs.sync.mkdir(imgPath)

		await Fs.async.writeFile(screenLandPath, await screenLand.getBufferAsync())
		await Fs.async.writeFile(screenPortPath, await screenPort.getBufferAsync())
		




		// Copiar iconos
		await this.copyIcons()

		// Copiar screeens
		await this.copyScreens()
	}



	// Copiar iconos ...
	async copyIcons(){
		var iconPlaces= {
            "drawable-hdpi/icon.png":{
                width:57,
                height:57
            },
            "drawable-ldpi/icon.png":{
                width:36,
                height:36
            },
            "drawable-mdpi/icon.png":{
                width:48,
                height:48
            },
            "drawable-xhdpi/icon.png":{
                width:96,
                height:96
            }
        }

        var output= this.project.getOutputFor(this.platformName), buf
        var resourcePath= Path.join(this.workingProjectDir, "platforms", "android", "res"), iconSize
        for(var id in iconPlaces){
        	iconSize= iconPlaces[id]
			icon= output.getNearestIcon(iconSize)
			if(!icon){
				icon= D.Project.Icon.default
			}

			buf= await icon.convertToSize(iconSize)
			await Fs.async.writeFile(Path.join(resourcePath, id), buf)
        }
	}


	// Copiar screeens ...
	async copyScreens(){
		var iconPlaces= {
            "drawable-land-hdpi/screen.png":{
                width:800,
                height:480
            },
            "drawable-land-ldpi/screen.png":{
                width:320,
                height:200
            },
            "drawable-land-mdpi/screen.png":{
                width:480,
                height:320
            },
            "drawable-land-xhdpi/screen.png":{
                width:1280,
                height:720
            },
            "drawable-port-hdpi/screen.png":{
                width:480,
                height:800
            },
            "drawable-port-ldpi/screen.png":{
                width:200,
                height:300
            },
            "drawable-port-mdpi/screen.png":{
                width:320,
                height:480
            },
            "drawable-port-xhdpi/screen.png":{
                width:720,
                height:1280
            }
        }

        var output= this.project.getOutputFor(this.platformName), buf
        var resourcePath= Path.join(this.workingProjectDir, "platforms", "android", "res"), iconSize
        for(var id in iconPlaces){
        	iconSize= iconPlaces[id]
			icon= output.getNearestScreen(iconSize)
			if(!icon){
				icon= iconSize.width>iconSize.height? D.Project.Screen.landscape : D.Project.Screen.portrait
			}

			buf= await icon.convertToSize(iconSize, {
				keepAspect: true
			})
			await Fs.async.writeFile(Path.join(resourcePath, id), buf)
        }
	}

	
	//async compile()

	
}
export default AndroidPackager