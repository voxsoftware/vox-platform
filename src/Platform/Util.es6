
import Path from 'path'
import fs from 'fs'
var D = core.org.voxsoftware.Developing
var Fs= core.System.IO.Fs

class Util{

	static _copyDirectory(fs, src, dest, customWriter, prefix=''){


        if(!fs.exists(dest))
            fs.mkdir(dest)

        var paths= fs.readdir(src),path,stat, rpath, handled, cpath, dpath
        for(var i=0;i<paths.length;i++){
            path= paths[i]
            rpath= Path.join(src, path)
            cpath= Path.join(prefix, path)
            dpath= Path.join(dest,path)
            stat= fs.stat(rpath)

            if(customWriter){
                if(customWriter.canWrite(cpath)){
                    customWriter.write(cpath, rpath, dpath)
                    handled=true
                }
            }
            if(!handled){
                if(stat.isDirectory()){
                    Util._copyDirectory(fs, rpath, dpath, customWriter, cpath)
                }
                else{
                    fs.writeFile(dpath, fs.readFile(rpath))
                }
            }
        }
    }

    static async _copyDirectoryAsync(fs, src, dest, customWriter, prefix=''){


        if(!await fs.exists(dest))
            await fs.mkdir(dest)

        var paths= await fs.readdir(src)
        var path,stat, rpath, handled, cpath, dpath
        for(var i=0;i<paths.length;i++){
            path= paths[i]
            rpath= Path.join(src, path)
            cpath= Path.join(prefix, path)
            dpath= Path.join(dest,path)
            stat= await fs.stat(rpath)
            handled= false
            /*if(cpath=="submodules\\vox-core-clr\\src"){
                vw.info('here')
            }*/
            if(customWriter){

                if(customWriter.canWrite(cpath)){
                    await customWriter.write(cpath, rpath, dpath)
                    handled=true
                }
            }

            if(!handled){
                if(stat.isDirectory()){
                    await Util._copyDirectoryAsync(fs, rpath, dpath, customWriter, cpath)
                }
                else{
                    await fs.writeFile(dpath, await fs.readFile(rpath))
                }
            }
        }
    }

    static remove(path){
        var fs= Fs.sync
        return Util.__remove(fs, path)
    }

    static removeAsync(path){
        var fs= Fs.async
        return Util.__removeAsync(fs, path)
    }

    static __remove(fs,path) {

    
        if( fs.exists(path) ) {
            if(fs.lstat(path).isDirectory()) {
                fs.readdir(path).forEach(function(file,index){
                    var curPath = path + "/" + file
                    if(fs.lstat(curPath).isDirectory()) { // recurse
                        Util.__remove(fs,curPath)
                    } else { // delete file
                        fs.unlink(curPath)
                    }
                })
                fs.rmdir(path)
            }
            else{
                fs.unlink(path)
            }

        }
    }

    static async __removeAsync(fs,path) {
    

        var paths,file, retry
        //vw.warning(fs)
        //vw.warning(path)
        if(await fs.exists(path) ) {

            if((await fs.stat(path)).isDirectory()) {
                paths= await fs.readdir(path)
                //paths.forEach(function(file,index){
                //vw.warning(paths)
                for(var index=0;index<paths.length;index++){
                    file= paths[index]
                    var curPath = path + "/" + file
                    if((await fs.lstat(curPath)).isDirectory()) { // recurse
                        //vw.warning(curPath)
                        await Util.__removeAsync(fs, curPath)
                    } else { // delete file
                        //vw.warning(curPath)
                        await fs.unlink(curPath)
                    }
                }

                try{
                    await fs.rmdir(path)
                }
                catch(e){
                    //vw.log(VoxPlatform.Fs.sync.readdir(path))
                    retry=true
                    await core.VW.Task.sleep(50)
                }
                if(retry)
                    await fs.rmdir(path)
            }
            else{
                await fs.unlink(path)
            }
        }
    }

    static copyDirectory(src,dest, customWriter){
        var fs= Fs.sync
        return this._copyDirectory(fs, src, dest, customWriter)
    }

    static copyDirectoryAsync(src,dest, customWriter){
        var fs= Fs.async
        return this._copyDirectoryAsync(fs, src, dest, customWriter)
    }

    static async gitConfigAsync(dir){

        var fs= Fs.async
        var config, content, file= Path.join(dir, ".git", "config"),e
        if(await fs.exists(file)){
            content=await fs.readFile(file,'utf8')
            config= ini.parse(content)
            if(config){
                if(e=config['remote "origin"'])
                    return e.url
            }
        }
    }


	static parseIgnore(line){
		var ignore={}
		if(line && !line.startsWith("#")){

			ignore.directory= line.endsWith("/")
			ignore.globalsearch= line.indexOf("/")<0
			ignore.negate= line.startsWith("!")
			while((y= line.indexOf("\\"))>=0){
                //vw.info('"'+line.substring(y, 2)+'"'  )
				line=  line.substring(0, y) +  JSON.parse('"'+line.substring(y, 2)+'"') + line.substring(y+2)
			}


			// En el caso que empiece por / se quita
			// se considera un ignore referente a directorio 
			// pero el pattern queda sin el / inicial
			if(line.startsWith("/"))
				line= line.substring(1)
            if(ignore.directory)
                line= line.substring(0,line.length-1)

			ignore.pattern= line
			
		    return ignore
        }
		
	}
	
	static parseIgnores(content){
        content= content.toString()
		var ignores=[], ignore, line, cache={}
		var lines= content.split(/[\r\n|\n|\r]/ig), y
		for(var i=0;i<lines.length;i++){
			ignore= {}
			line= lines[i].trim()
			ignore= Util.parseIgnore(line)
            if(ignore)
			 ignores.push(ignore)
		}


		return ignores

	}	

}

export default Util