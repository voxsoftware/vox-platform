import Path from 'path'
import Cp from 'child_process'
var D= core.org.voxsoftware.Developing
class Cordova extends D.Configuration{

	get path(){
		return this.parent.cordovaPath
	}


	get home(){
		return this.parent.cordovaHome
	}


	async executeAsync(args,options={}){
        var pro= await this.execute(args,options)
        var errors=[], results=[], err=[]

        pro.on('error', function(er){
            err.push(er)
        })
        pro.stdout.on("data", function(val){
            if(options.emitter)
                options.emitter.emit("cordova:stdout", val)
            results.push(val.toString())
        })
        pro.stderr.on("data", function(er){
            if(options.emitter)
                options.emitter.emit("cordova:stderr", er)
            errors.push(er.toString())
        })

        await pro.awaitToClose()
        var item, exc
        if(options.stderrAsError){
            while(item= errors.shift())
                err.push(item)
        }

        if(err.length>0){
            exc= new D.Platform.CordovaExecutionException('Error al ejecutar cordova. ' + err.join("|"))
            exc.errors= err
            throw exc
        }

        return {
            stderr:errors,
            stdout:results
        }
    }

    execute(args, options={}){
        var bin= this.path
        if(!bin)
            throw new core.System.Exception("No está instalado cordova. Por favor use npm -g install cordova para instalar")

        options.env= options.env || this.config.env
        if(!options.cwd){
            /*
            if(!options.project)
                options.cwd= this.config.workingDir
            else
                options.cwd= this.directory
            */
        }

        var p= Cp.spawn(bin, args, options)
        p.awaitToClose= this._awaitToClose
        return p
    }

    _awaitToClose(){
        var task= new core.VW.Task()
        this.on('close', function(ev){
            task.finish()
        })
        return task
    }

    /*
    get workingDir(){
    	return this.parent.directoryName
    }


    get workingProjectDir(){
    	return Path.join(this.workingDir, ".cordova")
    }
    */



	
}
export default Cordova