
// Configuration es para representar COnfiguraciones a partir de archivos 
import Path from 'path'
import {EventEmitter} from 'events'
var Fs= core.System.IO.Fs

class Configuration extends EventEmitter{
	
	
	constructor(fileOrJson, parent){

		super()
		this.$= this.$ ||{}
		if(typeof fileOrJson == "string"){

			var stat= Fs.sync.stat(fileOrJson)
			if(stat.isDirectory())
				fileOrJson= Path.join(fileOrJson, this.defaultFileName||"configuration.json")

			this.$.json= require(fileOrJson)
			if(this.$.json.default)
				this.$.json= this.$.json.default
			this.$.file= fileOrJson
		}
		else{
			this.$.json= fileOrJson
		}

		this.$.parent= parent


	}


	get(name){
		var j= this.$.json
		var i, parts= name.split(".")
		i=0
		
		while(j && i<parts.length){
			//vw.log(parts[i])
			j=j[parts[i]]
			i++
		}

		return j
	}


	set(name, val){
		var j= this.$.json
		var parts= name.split("."), part

		for(var i=0;i<parts.length;i++){
			part= parts[i]
			if(i==parts.length-1)
				j[part]= val
			else if(!j[part])
				j[part]= {}
		}
		return this
	}


	get parent(){
		return this.$.parent
	}

	get root(){
		var root= null, p= this
		while(p){
			root= p
			p= p.parent
		}	
		return root
	}

	get filename(){
		return this.$.file
	}

	get directoryName(){
		if(!this.$.dir){
			//vw.log(this.$)
			this.$.dir= Path.dirname(this.filename)
		}

		return this.$.dir
	}


	toJSON(){
		return this.$.json
	}



	save(){
		var file= this.filename
		var p= this.parent, lp
		if(!file){
			while(!file && p){
				file= p.filename
				if(file)
					lp=p
				p= p.parent
			}

			if(lp)
				return lp.save()

		}

		if(!file)
			throw new core.System.Exception("No se encontró el archivo de configuración")

		var data= core.safeJSON.stringify(this, null, '\t')
		if(file.endsWith(".es6") || file.endsWith(".js")){
			data= "module.exports= " + data
		}
		else if(!file.endsWith(".json")){
			throw new core.System.NotImplementedException("El formato del archivo" + file + " no es soportado para guardar.")
		}
		Fs.sync.writeFile(file, data)
		return this
	}


}

export default Configuration
