

var D= core.org.voxsoftware.Developing
class Output extends D.Configuration{
	
		
	get platform(){
		return this.get("platform")
	}

	set platform(platform){
		return this.set("platform", platform)
	}

	get command(){
		return this.get("command")
	}

	set command(cmd){
		return this.set("command", cmd)
	}


	get icons(){

		var icons= this.get("icons"), icon
		if(!icons)
			return []


		for(var i=0;i<icons.length;i++){
			icon= icons[i]
			if(!(icon instanceof D.Project.Icon))
				icon= new D.Project.Icon(icon, this)

			icons[i]= icon
		}

		return icons
	}

	get screens(){

		var screens= this.get("screens"), icon
		if(!screens)
			return []


		for(var i=0;i<screens.length;i++){
			screen= screens[i]
			if(!(screen instanceof D.Project.Screen))
				screen= new D.Project.Screen(screen, this)

			screens[i]= screen
		}

		return screens

	}



	getNearestIcon(size){

		// Obtiene el icono más cercano al tamaño especificado ...
		var icons= this.icons
		var dif1=999999, icong, iconl, ic, dif2
		dif2= dif1
		for(var i=0;i<icons.length;i++){
			ic= icons[i]
			if(ic.width > size.width){
				if(ic.width-size.width<dif2){
					icong= ic
					dif2= ic.width-size.width
				}
			}
			else if(size.width>ic.width){
				if(size.width-ic.width<dif1){
					iconl= ic
					dif1= size.width-icon.width
				}
			}
			else{
				return ic
			}
		}


		return icong||iconl
	}

	getNearestScreen(size){

		// Obtiene el icono más cercano al tamaño especificado ...
		var landScape= size.width>size.height

		var icons= this.screens
		//vw.info(icons)
		var dif1=999999, icon, ic, dif
		for(var i=0;i<icons.length;i++){
			ic= icons[i]
			dif= Math.abs(ic.width-size.width)+Math.abs(ic.height-size.height)
			if(dif<dif1){
				icon= ic
				dif1= dif
			}
		}

		return icon
	}


}

export default Output